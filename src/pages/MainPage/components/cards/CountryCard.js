import { useNavigate } from 'react-router-dom'

let wrapper = {
  border: '1px solid',
  margin: '10px auto',
  padding: '10px',
  width: '25%',
}

const Component = ({ item }) => {
  const navigate = useNavigate()
  const onClick = () => navigate(`/${item.name.common}`)
  
  return (
    <div style={wrapper}>
      <div>Country: {item.name.common}</div>
      <div>Languages:</div>
      <img alt="" src={item.coatOfArms.png} style={{ width: '20%' }}></img>
      <button onClick={onClick}>details</button>
    </div>
  )
}

export default Component
