import { useState, useEffect } from 'react'
import axios from 'axios'
import Component from './components/cards/CountryCard'

const ListPageComponent = () => {
  const [country, setCountry] = useState([])
  const [loading, setLoading] = useState(true)

  const load = async () => {
    const { data } = await axios.get('https://restcountries.com/v3.1/all')
    setCountry(data)
    setLoading(false)
  }

  useEffect(() => {
    load()
  }, [])

  if (loading) {
    return <>Loading...</>
  }

  return (
    <div>
      {country.map((data, index) => (
        <Component key={data?.name?.common} item={data} />
      ))}
    </div>
  )
}

export default ListPageComponent
