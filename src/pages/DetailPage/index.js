import { useEffect, useState } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'

const DetailPageComponent = () => {
  const { name } = useParams()
  const [country, setCountry] = useState({})

  const load = async () => {
    const { data } = await axios.get(`https://restcountries.com/v2/name/${name}`)
    console.log(data)
    setCountry(data[0])
  }

  useEffect(() => {
    load()
  }, [name])

  return (
    <div>
      detail page {name}
      {country && JSON.stringify(country)}
    </div>
  )
}

export default DetailPageComponent
