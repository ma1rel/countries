import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import MainPageComponent from './pages/MainPage'
import DetailPageComponent from './pages/DetailPage'

const router = createBrowserRouter([
  {
    path: '/',
    element: <MainPageComponent />,
  },
  {
    path: '/:name',
    element: <DetailPageComponent />,
  },
])

const Router = () => <RouterProvider router={router} />

export default Router
